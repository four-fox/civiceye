<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpotifyUsers extends Model
{
    use HasFactory;

    public function spotifyRatings()
	{
		return $this->hasMany("App\Models\UserRating",'source_id');
	}
}
